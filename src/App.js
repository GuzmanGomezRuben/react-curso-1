import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Button } from "react-bootstrap";

import AboutUs from "./pages/AboutUs";
import Contact from "./pages/Contact";
import Examples from "./pages/Examples";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h2>Router</h2>
      <Router>
        <div>
          <Link to="/">
            <Button>Home</Button>
          </Link>
          <Link to="/about-us">
            <Button>Quienes somos</Button>
          </Link>
          <Link to="/contact">
            <Button>Contacto</Button>
          </Link>
          <Link to="/examples">
            <Button>Ejemplos</Button>
          </Link>
        </div>

        <Switch>
          <Route path="/about-us">
            <AboutUs />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/examples">
            <Examples />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
