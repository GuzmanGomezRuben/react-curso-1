import React from "react";
import { Button } from "react-bootstrap";

export default function Greeting(props) {
  const { user, onClick } = props;
  return (
    <div>
      <h3>
        <Button onClick={() => onClick(user)}> Saludar </Button>
      </h3>
    </div>
  );
}
