import React from "react";
import Greeting from "./../components/Greeting";
import { Accordion, Card, Button } from "react-bootstrap";
import { ReactComponent as ReacIcon } from "./../assets/logo.svg";
import { useState, useEffect } from "react";

export default function Examples() {
  const user = {
    name: "Ruben Guzman",
    age: 29,
    color: "Azul",
  };

  const [stateCar, setStateCar] = useState(false);
  const [count, setStateCount] = useState(0);

  const onClick = (user) => {
    const { name = "Anonimo", age = 20 } = user;
    console.log(`Hola ${name} tiene ${age} años.`);
  };

  const turnOn = () => {
    setStateCar((prevValue) => !prevValue);
    setStateCount((prevValue) => prevValue + 1);
  };

  useEffect(() => {
    console.log("Total: ", count);
  }, [count]);
  return (
    <>
      <h3>El coche esta: {stateCar ? "Encendido" : "Apagado"}</h3>
      <h3>Clicks: {count}</h3>
      <Button onClick={turnOn}>Encender/Apagar</Button>
      <Greeting user={user} onClick={onClick} />
      <ReacIcon />

      <Accordion defaultActiveKey="0">
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Click me!
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>Hello! I'm the body</Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="1">
              Click me!
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="1">
            <Card.Body>Hello! I'm another body</Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
}
